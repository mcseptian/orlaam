// Include Core gulp
const gulp = require('gulp')

// Include Plugins
const autoprefixer = require('gulp-autoprefixer')
const minify = require('gulp-cssnano')
const eslint = require('gulp-eslint')
const header = require('gulp-header')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
const shell = require('gulp-shell')
const size = require('gulp-size')
const pkg = require('./package.json')
const del = require('del')
const bs = require('browser-sync').create()

// Include Info
const banner = [
    '/**',
    ' * <%= pkg.name %> - <%= pkg.description %>',
    ' * @version v<%= pkg.version %>',
    ' * @homepage <%= pkg.homepage %>',
    ' * @copyright ' + new Date().getFullYear() + ' <%= pkg.author.name %> ',
    ' * @license <%= pkg.license %>',
    ' */',
    '\n'
].join('\n')

// Add files directory
const files = {
    lint: ['Gulpfile.js', 'package.json'],
    sass: ['src/sass/**/*.scss'],
    css: ['dist/style.css'],
    clean: [
        'dist/orlaam.css',
        'dist/orlaam.min.css',
        'docs/assets/css/orlaam.css',
        'docs/*.html'
    ],
    doc: ['docs/assets/css/orlaam.css'],
    md: ['src/sass/**/*.md']
}

// Clean
gulp.task('clean', function () {
    files.clean.map(function (x) {
        console.log('Deleting ' + x)
    })
    return del(files.clean)
})

// Compile Sass
gulp.task('compile', function () {
    return gulp
        .src('src/sass/main.scss')
        .pipe(
            sass({
                style: 'expanded',
                quiet: false,
                cacheLocation: '.sass-cache'
            })
        )
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['> 0.1%', 'last 2 version'],
            cascade: false
        }))
        .pipe(rename('orlaam.css'))
        .pipe(gulp.dest('dist'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minify({
            keepSpecialComments: 0
        }))
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(gulp.dest('dist'))
        .pipe(
            size({
                gzip: true
            })
        )
        .pipe(rename('style.css'))
        .pipe(gulp.dest('docs/assets/css'))
        .pipe(bs.reload({
            stream: true
        }))
})

// Lint
gulp.task('eslint', function () {
    return (gulp
        .src(files.lint)
        .pipe(eslint.format())
        .pipe(eslint.failOnError()))
})

// Documenting
gulp.task('docs',
    shell.task(
        ['yarn kss --config kss-config.json']
    )
);

gulp.task('browser-sync', function () {
    bs.init({
        server: {
            baseDir: 'docs'
        },
        open: false,
        ui: {
            port: 8000,
            weinre: {
                port: 9000
            }
        },
        port: 7000
    })
})

// Watch Files For Changes
gulp.task('watch', ['browser-sync'], function () {
    gulp.watch(files.sass, ['compile', 'docs']).on('change', bs.reload)
})

// Default Task
gulp.task('default', function () {
    gulp.start(['clean', 'eslint', 'compile', 'docs'])
})
